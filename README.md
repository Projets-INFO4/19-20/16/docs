# Contribution to EdCampus software

Final report and demonstration video are situated in folder Presentations->Final Presentation

## Goals

1. Try to optimize the EdCampus code from a "Green IT" perspective.
2. Try to find a way to measure the ecological impact of software and make EdCampus a textbook case for Green IT. (represents about 3/4 of the project ~ 11 sessions)
3. Before embarking on this, perform maintenance on the application (bug fixing) in order to familiarize with the code. (about 1/4 of the project ~ 2 sessions)

## Project Tracking

### 20/01

Discovery of projects and allocation of groups

### 27/01

Meeting with Anthony Geourjon, discovery of EDCampus and discussion of the project objectives

### 03/02

Installation of EDCampus development tools and platform

*Encountered problems:* npm installation failed on Élisa's computer

### 10/02

Discovering and understanding the code, reading the book *Eco-conception web : les 115 bonnes pratiques*
Élisa still have problems with npm, so the code review was on Louis computer.

### 17/02

Npm problems fixed + Book's reading finished + Writing of the 115 good practices from the book to an Excel document 

### 02/03

Fill in the document using the application code

### 09/03

Mid-Project defense

### 16/03

1. Git repository was created for the code of our new application, Ecoscore. This application will allow EDCampus as well as any web application using PHP and JavaScript to verify compliance with recommended practices for saving resources.
2. Start of the application programming phase.

### 17/03

Ecoscore programming

### 23/03

1. Ecoscore programming
2. Code review via Chrome's GreenIT-analysis plugin with a fiber connection: analysis of each web pages from a student view and writing the results in an Excel document

### 24/03

1. Ecoscore programming
2. Code review via Chrome's GreenIT-analysis plugin with a fiber connection: analysis of each web pages from a tutor view and writing the results in an Excel document

### 30/03

1. Ecoscore programming
2. Code review via Chrome's GreenIT-analysis plugin with a fiber connection: analysis of each web pages from an admin view and writing the results in an Excel document

### 31/03

1. Code review via Chrome's GreenIT-analysis plugin with a fiber connection: analysis of each web pages in the English version of the application from all the possible profiles and writing the results in an Excel document
2. Reorganization of the results excel document: results for pages that are common to each profile, results for pages that are common to tutors view and students view, and results for pages that are specific to each profiles.

### 07/04

1. Compute an average Green-IT score via the results obtained with the GreenIT-analysis plugin. This average score was made with a fiber connection.
2. Writing a report on the results obtained via the plugin for a fiber optic connection.

### 15/04

Analysis of the pages of the application with Chrome's GreenIT-analysis module under a 4G connection and writing the results in an Excel document

### 27/04

1. Same as the previous session
2. Final writing of the report about the GreenIT-analysis results.
3. Analysis of the application via the chrome audit lighthouse plugin.

### 28/04

1. Translation in english of all the documents
2. Final writing of the report about the lighthouse audit results

### 29/04

1. ecoscore programming
2. Update the excel file about good practices

### 30/04

1. ecoscore programming
2. Writing of the report, presentation
3. Filming a demo video

