# Lighthouse analysis report

## What is Lighthouse Audit?

Lighthouse is an audit extension for Chrome. Lighthouse is a very useful tool for those interested in the performance and quality of web applications. To generate an audit, it is possible to launch Lighthouse via Chrome DevTools, from the command line or as a Node Module. The principle is simple, Lighthouse takes as argument the URL of the application to be examined and generates a results report. The main criteria are performance, accessibility, progressive web apps, SEO. This tool is quite interesting because it does not only point to the failures of an application, but furnishes also documentation that explains how to resolve these failures.

## Lighthouse on EdCampus

We ran the Lighthouse tool on the EdCampus application. We will report here about the audit report and try to deduce a possible ecological impact when it is not explicitly mentioned in the report.

## Performance

The score of **0/100** is assigned to the EdCampus application in terms of performance. Given the results, the application seems very slow. With a fiber connction of more than 100Mb/s, the first contentful paint (text or image) to be loaded was loaded in more than 43s and the first meaningful paint appeared in 44,4s. The application content was visibly populated in about 48s. In a little more than 44s the main thread was quiet enough to handle input and a maximal first input delay a user could experience was 340ms. The statistics shows that the app was fully interactive in about 48s. These performances are way too slow and a slow loading means a biggest resources consumption. Lighthouse's advices for improving performance is to remove unused css, and eliminate render-blocking resources. These two actions could save loading times by around 50s. 321 resources were found to be static assets. An efficient cache policy with a long cache lifetime could speed up repeat visits on the app and by the way serve these static assets. The main-thread work could also be minimize by considering to reduce the time spent parsing, compiling and executing JS. It is estimated that 16s could be saved.

## Accessibility

EdCampus scores a grade of **88/100** in terms of accessibility, which is a pretty good grade. The faults found here are mainly problems of contrast between the background and the text and referencing problems on links and labels. These problems can therefore be solved quite simply to make the application accessible to the greatest number of people. Nevertheless, these modifications will have no ecological impact.

## Best practices

The score for the respect of best practices is **79/100** It seems that 325 requests were not served via HTTP/2, however it is known that HTTP/2 offers many benefits over HTTP/1.1, including binary headers, multiplexing, and server push. HTTP/2 serves the application resources faster with less data moving over the wire. It is a great and easy way to reduce the ecological footprint of the application. The function *document.write()* in invocated in a js. External scripts dynamically injected via *document.write()* can delay page load by tens of seconds for user with a slow connection. Lighthouse indicates that Chrome therefore blocks the execution of *document.write()* in many cases, meaning you can't rely on it (source: https://developers.google.com/web/updates/2016/08/removing-document-write). It is therefore useless to try to use this function and therefore consume resources. 

## Progressive Web App

The application does not work online and once again, app load is not fat enough on mobile networks. As said in the **Performances** part, the app began to be Interactive at about 48s.

## Conclusion

This analysis highlights the fact that the app loading is too slow. The more the load is slow the more it consumes. So it is the main thing to improve to respect a GreenIT approach.