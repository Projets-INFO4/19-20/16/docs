# TO DO LIST

* [x]  Lire 115 bonnes pratiques
* [x]  Excel 115 bonnes pratiques
* [x]  Analyse du code
* [x]  Review GreenIT - connexion fibre
* [x]  Rapport GreenIT
* [x]  Créer ecoparser
* [x]  Réorganiser le git
* [x]  GreenIT analysis - connexion 4G
* [x]  Mettre à jour le rapport GreenIT
* [x]  Traduire en Anglais les docs
* [x]  Tester + Rédiger un rapport résultats LightHouse
* [x]  Mettre à jour 115 bonnes pratiques
* [x]  Finir ecoparser
* [x]  Rédiger Rapport Final
* [x]  Faire diapo
* [x]  Faire vidéo de présentation